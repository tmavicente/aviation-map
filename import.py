## [Countries] // Tem ident duplicado

## [Airports] // Tem ident duplicado
## ;ICAO|Airport Name|Latitude Decimal|Longitude Decimal|IATA/LID|FIR|IsPseudo

## [FIRs] // Tem ident duplicado
## ;ICAO|NAME|CALLSIGN PREFIX|FIR BOUNDARY

## [UIRs] // Nao tem ident duplicados

## [IDL] ??

import json
class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        else:
            return super().default(obj)
        
class VatSpyParser:
    def __init__(self, filepath='VAT-Spy/VATSpy.dat'):
        with open(filepath) as file:
            data = file.read()

        self.data = dict()

        sectors = data.split('\n\n')
        for sector in sectors:
            key, values = self._split_sector_data(sector)
            self.data[key] = values
    
    def _split_sector_data(self, sector):
        lines = sector.strip().split('\n')
        lines = [l for l in lines if not l.startswith(';')]
        line_tag = lines.pop(0)
        return line_tag, lines

    def _split_uir_line(self, line):
        args = line.split('|')
        return {
            'icao': args[0],
            'name': args[1],
            'firs': args[2].strip().split(','),
        }

    def _split_airport_line(self, line):
        args = line.split('|')
        return {
            'icao': args[0],
            'name': args[1],
            'lat': float(args[2]),
            'lng': float(args[3]),
            'iata_code': args[4],
            'fir': args[5],
            'isPseudo': bool(int(args[6][0]))
        }

    def _split_fir_line(self, line):
        args = line.split('|')
        return {
            'icao': args[0],
            'name': args[1],
            'callsign_prefix': args[2],
            'fir_boundary': args[3],
        }

    def parse(self):
        output = dict()
        for key, lines in self.data.items():
            match key:
                case '[UIRs]':
                    output['uirs'] = list(map(self._split_uir_line, lines))
                case '[Airports]':
                    output['airports'] = list(map(self._split_airport_line, lines))
                case '[FIRs]':
                    output['firs'] = list(map(self._split_fir_line, lines))
        return output

airports = dict()
firs = dict()
uirs = dict()

def update_airport(icao, name, lat, lng, iata_code, fir, *args, **kwargs):
    airport = {
        'icao': icao,
        'name': name,
        'lat': float(lat),
        'lng': float(lng),
        'iata_codes': set(),
        'fir': fir,
    }
    airport = airports.get(icao) or airport
    airport['iata_codes'].add(iata_code or icao)
    airports[icao] = airport

def update_fir(icao, name, callsign_prefix, fir_boundary):
    fir = {
        'icao': icao,
        'name': name,
        'callsign_prefix': callsign_prefix,
        'fir_boundary': fir_boundary if fir_boundary else icao,
    }
    fir = firs.get(icao) or fir
    firs[icao] = fir

    if callsign_prefix:
        fir = {
            'icao': callsign_prefix,
            'name': name,
            'callsign_prefix': callsign_prefix,
            'fir_boundary': fir_boundary if fir_boundary else icao,
        }
        fir = firs.get(callsign_prefix) or fir
        firs[callsign_prefix] = fir

def update_uir(icao, name, firs):
    uir = {
        'icao': icao,
        'name': name,
        'firs': firs,
    }
    uir = uirs.get(icao) or uir
    uirs[icao] = uir

for key, values in VatSpyParser().parse().items():
     match key:
        case 'uirs':
            [update_uir(**x) for x in values]
            with open('static/compiled/uirs.json', 'w') as file:
                file.write(json.dumps(list(uirs.values()), indent=4))
        case 'airports':
            [update_airport(**x) for x in values]
            with open('static/compiled/airports.json', 'w') as file:
                file.write(json.dumps(list(airports.values()), indent=4, cls=CustomEncoder))
        case 'firs':
            [update_fir(**x) for x in values]
            with open('static/compiled/firs.json', 'w') as file:
                file.write(json.dumps(list(firs.values()), indent=4))

print(f'{len(airports.values())} airports imported.')
print(f'{len(firs.values())} firs imported.')
print(f'{len(uirs.values())} uirs imported.')
print('FIM')