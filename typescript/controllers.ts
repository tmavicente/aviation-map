class ControllerCallsign {
  prefix: string
  variable: string
  suffix: string
  raw: string
  stationType: STATION_TYPES

  constructor(callsign: string) {
    let parts = callsign.split('_')

    this.prefix = parts[0],
    this.variable = parts.length > 2 ? parts[1] : '',
    this.suffix = parts[parts.length - 1],
    this.raw = callsign

    let stationType = this._getStationType(this.suffix, this.prefix)
    this.stationType = stationType
  }

  _getStationType(suffix: string, prefix: string) {
    if(suffix == 'FSS'){
      let isUIR = REPOSITORY.uirs.find((e: any) => e.icao == prefix) !== undefined
      return isUIR ? STATION_TYPES.UIR : STATION_TYPES.FIR
    }
    else if(suffix == 'CTR')
      return STATION_TYPES.FIR
    else if(suffix == 'APP' || suffix == 'DEP' )
      return STATION_TYPES.APP
    return STATION_TYPES.AIRPORT
  }
}

class Controller {
  callsign: ControllerCallsign
  name: string
  frequency: string

  constructor (callsign: ControllerCallsign, name: string, frequency: string) {
    this.callsign = callsign
    this.name = name
    this.frequency = frequency
  }

  isStation() { 
    return ['ATIS', 'DEL', 'GND', 'TWR', 'DEP', 'APP', 'CTR', 'FSS'].indexOf(this.callsign.suffix) > -1
  }
}

class ControllerFactory {
  build(callsign: string, name: string, frequency: string) {
    return new Controller(new ControllerCallsign(callsign), name, frequency);
  }
}
