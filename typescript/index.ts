var mapCenter = new L.LatLng(0, 0)
var mapZoom = 2
var mapTileLayerUrl = 'https://cartodb-basemaps-a.global.ssl.fastly.net/dark_nolabels/{z}/{x}/{y}.png'
var map = new AviationMap(mapCenter, mapZoom, mapTileLayerUrl)

class VatsimData {
  stations: Station[]
  pilots: Pilot[]

  constructor(data: any) {
    // handle invalid data
    data = data ? data : []

    // controller stations
    let controllerFactory = new ControllerFactory();
    let stationsBuilder = new StationsBuilderFactory().stationsBuilder(REPOSITORY)
    // exemplo de teste
    // data.controllers.push(
    //     {
    //       "cid": '00000',
    //       "name": "Tester",
    //       "callsign": "EURM_FSS",
    //       "frequency": "199.998",
    //       "facility": 3,
    //       "rating": 1,
    //       "server": "UK",
    //       "visual_range": 50,
    //       "text_atis": null,
    //       "last_updated": "2023-04-24T16:00:11.3854336Z",
    //       "logon_time": "2023-04-24T10:11:38.315001Z"
    //       },
    // )
    data.controllers.
      concat(data.atis).
      map((e: any) => controllerFactory.build(e.callsign, e.name, e.frequency)).
      filter((e: Controller) => e.isStation()).
      forEach((e: Controller) => stationsBuilder.addController(e))
    
    // pilots
    let pilots = data.pilots.map((e: any) => new Pilot(
      e.callsign,
      e.name, 
      e.latitude, 
      e.longitude, 
      e.altitude,
      e.groundspeed,
      e.transponder,
      e.heading,
    ))

    this.stations = stationsBuilder.build();
    this.pilots = pilots;
  }
}

async function FetchFromAPI(){
  let boundaries;
  await fetch('./static/compiled/Boundaries.geojson')
    .then(response => response.json())
    .then(data => boundaries = data.features)
  
  let firs;
  await fetch('./static/compiled/firs.json')
    .then(response => response.json())
    .then(data => firs = data)
  
  let uirs;
  await fetch('./static/compiled/uirs.json')
    .then(response => response.json())
    .then(data => uirs = data)
  
  let airports;
  await fetch('./static/compiled/airports.json')
    .then(response => response.json())
    .then(data => airports = data)
  
  let appBoundaries;
  await fetch('./static/traconboundaries.json')
    .then(response => response.json())
    .then(data => appBoundaries = data.features)  

  return {
    boundaries: boundaries,
    firs: firs,
    uirs: uirs,
    airports: airports,
    appBoundaries: appBoundaries
  }
}

async function FetchFromVATSIM(){
  await fetch('https://data.vatsim.net/v3/vatsim-data.json')
    .then(response => response.json())
    .then((data: any) => {
      console.log('Update from VATSIM...')

      data = new VatsimData(data);

      map.drawStations(data.stations)
      map.drawPilots(data.pilots)

      console.log('Updated from VATSIM\t==========================')
    })
}

var REPOSITORY: any;
FetchFromAPI()
  .then(e => REPOSITORY = e)
  .then(e => {
    FetchFromVATSIM()
    setInterval(FetchFromVATSIM, 1000 * 30)
  })


