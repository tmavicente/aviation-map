type UIR = {
  icao: string
  name: string
  firs: string[]
}

type FIR = {
  icao: string
  name: string
  callsign_prefix: string
  fir_boundary: string
}

enum STATION_TYPES {
  AIRPORT,
  APP,
  FIR,
  UIR,
}

class Station {
  type: STATION_TYPES
  controllers: Controller[]
  icao: string

  constructor(icao: string, controller: Controller) {
    this.controllers = [controller,]
    this.icao = icao
    this.type = controller.callsign.stationType
  }

  addController(controller: Controller): void {
    this.controllers.push(controller)
  }
}

class AirportStation extends Station {
  lat: number
  lng: number

  constructor(icao: string, lat: number, lng: number, controller: Controller) {
    super(icao, controller)
    this.lat = lat
    this.lng = lng
  }
}

class UpperStation extends Station {
  geometries: GeoJSON.GeoJsonObject[]

  constructor(icao: string, geometries: GeoJSON.GeoJsonObject[], controller: Controller) {
    super(icao, controller)
    this.geometries = geometries
  }
}

class StationGeometriesFactory {
  repo: any

  constructor(repo: any) {
    this.repo = repo
  }

  #getAppGeometries(callsign: ControllerCallsign) : GeoJSON.GeoJsonObject[] {
    let geometries = this.repo.appBoundaries.filter((e: GeoJSON.Feature) =>
      e.properties?.id == callsign.prefix ||
      e.properties?.prefix.indexOf(callsign.prefix) > -1
    ).map((e: GeoJSON.Feature) => e.geometry )
    if(!geometries.length)
      throw new Error('Can not get geometries from callsign: ' + callsign.raw)
    return geometries
  }

  #getFirGeometries(callsign: ControllerCallsign) : GeoJSON.GeoJsonObject[] {
    // Localizar FIR
    let fir = this.repo.firs.find((fir: FIR) => 
      fir.icao == callsign.prefix + '-' + callsign.variable ||
      fir.icao == callsign.prefix
    )
    if(!fir)
        throw new Error('Can not get FIR from callsign: ' + callsign.raw)
    
    // Localizar geometrias
    let geometries: Array<GeoJSON.Feature> = this.repo.boundaries.filter(
      (e: GeoJSON.Feature) =>
        fir.fir_boundary == e.properties?.id
    ).map((e: GeoJSON.Feature) => e.geometry )

    if(!geometries.length)
      throw new Error('Can not get geometries from callsign: ' + callsign.raw)

    return geometries
  }

  #getUirGeometries(callsign: ControllerCallsign) : GeoJSON.GeoJsonObject[] {
    let geometries = new Array<GeoJSON.GeoJsonObject>()
    
    // Localizar UIR
    let uir: UIR = this.repo.uirs.find((e: UIR) => e.icao == callsign.prefix)
    if(!uir)
    throw Error(`unsupported UIR callsign: ${callsign.raw}`)
    
    // Localizar FIR icaos
    let firIcaos: string[] = uir.firs
    
    // Localizar geometrias das FIRS
    firIcaos.forEach((firIcao: string) => {
      // Localizar FIR
      let fir = this.repo.firs.find((fir: FIR) => 
        fir.callsign_prefix == firIcao ||
        fir.icao == firIcao
      )
      if(!fir)
          throw new Error('Can not get FIR from callsign: ' + firIcao)
          // Localizar geometrias
      let _geometry: GeoJSON.GeoJsonObject = this.repo.boundaries.find(
        (e: GeoJSON.Feature) => 
        fir.fir_boundary == e.properties?.id
      ).map((e: GeoJSON.Feature) => e.geometry)
      
      if(!_geometry)
        throw new Error('Can not get geometries from callsign: ' + callsign.raw)

      geometries.push(_geometry)
      
    });
    
    return geometries
  }

  geometries(callsign: ControllerCallsign) : GeoJSON.GeoJsonObject[] {
    if(callsign.stationType === STATION_TYPES.UIR)
      return this.#getUirGeometries(callsign)
    if(callsign.stationType === STATION_TYPES.FIR)
      return this.#getFirGeometries(callsign)
    return this.#getAppGeometries(callsign)
  }
}

class StationBuilder {
  repo: any
  geometriesFactory: StationGeometriesFactory

  constructor(repo: any) {
    this.repo = repo
    this.geometriesFactory = new StationGeometriesFactory(this.repo)
  }

  #buildAirportStation(controller: Controller) : AirportStation {
    let airport = this.repo.airports.find((e: any) =>
      e.iata_codes.indexOf(controller.callsign.prefix) > -1 || 
      e.icao == controller.callsign.prefix
    )
    let icao = controller.callsign.prefix + controller.callsign.stationType
    return new AirportStation(icao, airport.lat, airport.lng, controller)
  }

  build(controller: Controller) {
    if (controller.callsign.stationType == STATION_TYPES.AIRPORT) {
      return this.#buildAirportStation(controller)
    }
    else {
      let icao = controller.callsign.prefix + controller.callsign.stationType

      // Prevent not found geometries
      try {
        let geometries = this.geometriesFactory.geometries(controller.callsign)
        return new UpperStation(icao, geometries, controller)
      } catch (error) {
        // In case of Approach
        if(controller.callsign.stationType == STATION_TYPES.APP) {
          return this.#buildAirportStation(controller)
        }
        else{
          throw error;
        }
      }
    }
  }
}

class StationsBuilder {

  stations: Station[]
  builder: StationBuilder

  constructor (stationBuilder: StationBuilder) {
    this.builder = stationBuilder
    this.stations = []
  }

  addController(controller: Controller) {
    try {
      let icao = controller.callsign.prefix + controller.callsign.stationType
      let station = this.stations.find(e => e.icao == icao)
      if (!station) {
        this.stations.push(this.builder.build(controller))
      } else {
        station.addController(controller)
      }
    } catch (error) {
      console.error(error);
    }
  }

  build() {
    return this.stations
  }
}

class StationsBuilderFactory {
  stationsBuilder(repo: any) : StationsBuilder {
    return new StationsBuilder(this.stationBuilder(repo))
  }

  stationBuilder(repo: any) : StationBuilder {
    return new StationBuilder(repo)
  }
}
