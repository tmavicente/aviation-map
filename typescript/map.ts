class IconFactory {
  pilot(pilot: Pilot) : L.Icon {
    let anchorSize = 12
    let iconSize = 24
    if(pilot.altitude >= 10000 && pilot.altitude < 20000){
      anchorSize = 14
      iconSize = 28
    }
    else if(pilot.altitude >= 20000){
      anchorSize = 16
      iconSize = 32
    }

    return new L.Icon({
      iconUrl: "./static/img/B739.png",
      iconSize: [iconSize, iconSize],
      iconAnchor: [anchorSize, anchorSize],
    })
  }

  airport(station: Station) : L.DivIcon {
    return L.divIcon({
      className: 'airport-div-icon',
      html: this.#airportHtml(station),
    });
  }

  #airportHtml(station: Station){
    let letters = [...new Set(station.controllers.map(x => x.callsign.suffix[0]))];
    
    // get marginLeft
    let marginLeft = 0
    if(letters.length == 1) {
      marginLeft = -33
    }
    else if(letters.length == 2) {
      marginLeft = -110
    }
    else if(letters.length == 3) {
      marginLeft = -185
    }
    else if(letters.length == 4) {
      marginLeft = -260
    }

    let html = `<table style="margin-left: ${marginLeft}%; margin-top: -25%; font-size: 0.7rem; font-weight: bold"><tr>`
    letters.forEach(element => {
      if(element === 'D')
        html += `<td style='background-color:blue; color:#fff; text-align: center; padding: 0px 5px'>${element}</td>`
      else if(element === 'G')
        html += `<td style='background-color:green; color:#fff; text-align: center; padding: 0px 5px'>${element}</td>`
      else if(element === 'T')
        html += `<td style='background-color:red; color:#fff; text-align: center; padding: 0px 5px'>${element}</td>`
      else if(element === 'A')
        html += `<td style='background-color:orange; color:#fff; text-align: center; padding: 0px 5px'>${element}</td>`
      else
        html += `<td style='background-color:pink; color:#fff; text-align: center; padding: 0px 5px'>${element}</td>`
    });
    html += '<tr><table>'
    return html
  }
}

class PopUpFactory {
  pilot(pilot: Pilot) : L.Popup {
    let popUpContent = `
      <table>
        <tr>
          <td style='font-weight: bold;'>${pilot.callsign}</td>
        </tr>
        <tr>
          <td>${pilot.name}</td>
        </tr>
        <tr>
          <td>Alt</td>
          <td style='font-weight: bold; text-align: right;'>${pilot.altitude}ft</td>
        </tr>
        <tr>
          <td>Gs</td>
          <td style='font-weight: bold; text-align: right;'>${pilot.groundspeed}kt</td>
        </tr>
        <tr>
          <td>Hdg</td>
          <td style='font-weight: bold; text-align: right;'>${pilot.heading}º</td>
        </tr>
        <tr>
          <td>Sq:</td>
          <td style='font-weight: bold; text-align: right;'>${pilot.transponder}</td>
        </tr>
      </table>
    `
    return new L.Popup({pane: 'POPUP'}).setContent(popUpContent)
  }

  controller(station: Station) : L.Popup {
    let content = `
      <table>
        ${station.controllers.reduce(this.#controllerText, '')}
      </table>
    `
    
    station.controllers.reduce(this.#controllerText, '')
    return new L.Popup({pane: 'POPUP'}).setContent(content)
  }
  
  #controllerText(previousValue: string, currVal: Controller, index: number) : string {
    let content = `
      <tr>
        <td style='font-weight: bold;'>${currVal.callsign.raw}</td>
        <td style='font-weight: bold; text-align: right;'>${currVal.frequency}</td>
      </tr>
      <tr>
        <td>${currVal.name}</td>
      </tr>
    `
    return index == 0 ? content : `${previousValue}${content}`
  }
}

class LayerFactory {
  #popupFactory: PopUpFactory
  #iconFactory: IconFactory

  constructor(){
    this.#popupFactory = new PopUpFactory()
    this.#iconFactory = new IconFactory()
  }

  tile(url: string): L.TileLayer {
    return new L.TileLayer(url, { pane: 'MAIN' })
  }

  pilot(pilot: Pilot) : L.Marker {
    let icon = this.#iconFactory.pilot(pilot)
    let markerOptions = {icon: icon, pane: pilot.callsign, rotationAngle: pilot.heading}
    let popUp = this.#popupFactory.pilot(pilot)
    return new L.Marker([pilot.lat, pilot.lng], markerOptions).bindPopup(popUp)
  }

  controller(station: AirportStation | UpperStation) : L.GeoJSON | L.Circle | L.Marker {
    let popUp = this.#popupFactory.controller(station)

    if (station instanceof UpperStation) {
      let color = station.type == STATION_TYPES.UIR ? 'cyan' : station.type == STATION_TYPES.APP ? 'darkred' : 'blue'
      let options = { style : { color: color, pane: station.icao}}
      return L.geoJSON(station?.geometries, options).bindPopup(popUp)
    }
    // Only for approaches without geometries
    else if(station instanceof AirportStation && station.type == STATION_TYPES.APP) {
      let options = {radius: 50000, color: 'darkcyan', pane: station.icao}
      return new L.Circle([station.lat, station.lng], options).bindPopup(popUp)
    }
    else{
      let icon = this.#iconFactory.airport(station)
      let options = {icon: icon, pane: station.icao}
      return new L.Marker([station.lat, station.lng], options).bindPopup(popUp)
    }
  }
}

class ZIndexFactory {
  zIndexFromName(name: string) : string {
    switch(name)
    {
      case 'MAIN':
        return '-999'
      case 'POPUP':
        return '999999'
      default:
        throw new Error(`unsupported name: ${name}`)
    }
  }

  zIndexFromPilot(pilot: Pilot) : string {
    let baseIndex = 4000
    return Math.round(baseIndex + pilot.altitude).toString()
  }

  zIndexFromStation(station: AirportStation | UpperStation) : string {
    if(station.controllers.some(e=> e.callsign.stationType === STATION_TYPES.AIRPORT))
      return '3001'
    else if(station.controllers.some(e=> e.callsign.stationType === STATION_TYPES.APP))
      return '3000'
    else if(station.controllers.some(e=> e.callsign.stationType === STATION_TYPES.FIR))
      return Math.round(
        2000 - Math.max(...(station as UpperStation).geometries.map(e => this.#approximatePolygonArea(e)))
      ).toString()
    return Math.round(
      1000 - Math.max(...(station as UpperStation).geometries.map(e => this.#approximatePolygonArea(e)))
    ).toString()
  }

  #approximatePolygonArea(geometry: any) : number {
    if (geometry.type !== 'MultiPolygon') {
        throw Error(`Geometry type not supported: ${geometry.type}`)
    }
    let flatten = geometry.coordinates.flat(2);
    let abs = flatten.map((x: any) => x[0])
    let ord = flatten.map((x: any) => x[1])
    return Math.min(
          Math.max(...abs) - Math.min(...abs),
          Math.abs(Math.max(...abs) - (Math.min(...abs) + 90))
      ) *
      Math.min(
          Math.max(...ord) - Math.min(...ord),
          Math.abs(Math.max(...ord) - Math.min(...ord) + 180)
      )
  }
}

class PanesBuilder {
  #map: L.Map
  #zIndexFactory: ZIndexFactory
  #panes: Map<string, HTMLElement>

  constructor(map: L.Map){
    this.#map = map
    this.#zIndexFactory = new ZIndexFactory()
    this.#panes = new Map<string, HTMLElement>()
  }

  buildDefault() : void {
    ['MAIN', 'POPUP'].forEach((e: string) => 
      this.#map.createPane(e).style.zIndex = this.#zIndexFactory.zIndexFromName(e)
    )
  }

  buildStation(station: AirportStation | UpperStation) : void {
    let pane = this.#panes.get(station.icao) || this.#map.createPane(station.icao)
    pane.style.zIndex = this.#zIndexFactory.zIndexFromStation(station)
    this.#panes.set(station.icao, pane)
  }

  buildPilot(pilot: Pilot) : void {
    let pane = this.#panes.get(pilot.callsign) || this.#map.createPane(pilot.callsign)
    pane.style.zIndex = this.#zIndexFactory.zIndexFromPilot(pilot) 
    this.#panes.set(pilot.callsign, pane)
  }
}

class AviationMap {
  #map: L.Map
  groups: Map<string, L.LayerGroup>
  layerFactory: LayerFactory
  panesBuilder: PanesBuilder

  constructor(center: L.LatLng, zoom: number, tileLayerUrl: string){
    // Initialize map
    this.#map = L.map('map')
    this.#map.setView(center, zoom)
    
    this.groups = new Map<string, L.LayerGroup>()
    this.layerFactory = new LayerFactory()
    this.panesBuilder = new PanesBuilder(this.#map)
    
    // add default panes
    this.panesBuilder.buildDefault()

    // add Tile Layer
    this.layerFactory.tile(tileLayerUrl).addTo(this.#map);

    //  create layer controls
    let overlays = {
      Pilots: this.#getOrCreateGroup('pilots'),
      ATC: this.#getOrCreateGroup('controllers'),
    }
    L.control.layers({}, overlays, {collapsed: false}).addTo(this.#map);
  }

  #getOrCreateGroup(name: string) : L.LayerGroup {
    let group = this.groups.get(name)
    if(!group)
    {
      group = new L.LayerGroup().addTo(this.#map)
      this.groups.set(name, group)
    }
    return group 
  }

  drawStations(stations: any) : void {
    let layerGroup = this.#getOrCreateGroup('controllers')
    let layers = stations.map((e: any) => this.#createStationLayers(e))
    layerGroup.clearLayers()
    layers.forEach((e: any) => layerGroup.addLayer(e))
  }

  drawPilots(pilots: Pilot[]) : void {
    let layerGroup = this.#getOrCreateGroup('pilots')
    let markers = pilots.map((e: any) => this.#createPilotLayers(e))
    layerGroup.clearLayers()
    markers.forEach((e: any) => layerGroup.addLayer(e))
  }

  #createStationLayers(station: AirportStation | UpperStation) : L.GeoJSON | L.Circle | L.Marker {
    this.panesBuilder.buildStation(station)
    return this.layerFactory.controller(station)
  }

  #createPilotLayers(pilot: Pilot) : L.Marker {
    this.panesBuilder.buildPilot(pilot)
    return this.layerFactory.pilot(pilot)
  }
}