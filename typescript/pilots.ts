class Pilot {
    callsign: string
    name: string
    lat: number
    lng: number
    altitude: number
    groundspeed: number
    transponder: string
    heading: number

    constructor(
        callsign: string, 
        name: string,
        lat: number, 
        lng: number, 
        altitude: number,
        groundspeed: number,
        transponder: string,
        heading: number,
    ){
        this.callsign = callsign
        this.name = name
        this.lat = lat
        this.lng = lng
        this.altitude = altitude
        this.groundspeed = groundspeed
        this.transponder = transponder
        this.heading = heading
    }
}